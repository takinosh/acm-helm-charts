# Red Hat ACM Helm example on private repo


## RHACM 2.4 Create an Application from Bitbucket using ssh-keys
* https://access.redhat.com/solutions/6087231

##  Requirements 
* add ssh keey to https://bitbucket.org/account/settings/ssh-keys/

### Steps to deploy dev-ex-dashboard
![20211208231653](https://i.imgur.com/eF6hpdo.png)
**Create dev-ex-dashboard project and deploy ssh key**
```
oc new-project dev-ex-dashboard
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n dev-ex-dashboard
```

**Deploy to local cluster**
```
oc create -f acm-config/dev-ex-dashboard/deploy-local-cluster.yaml 
```

**Create dev-ex-dashboard-dev project and deploy ssh key**
```
oc new-project  dev-ex-dashboard-dev
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n  dev-ex-dashboard-dev
```

**Deploy to dev cluster**
```
oc create -f acm-config/dev-ex-dashboard/deploy-dev-cluster.yaml
```

### Steps to deploy dotnet-chart
**Create dotnet-chart project and deploy ssh key**
```
oc new-project dotnet-chart
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n dotnet-chart
```

**Deploy to local cluster**
```
oc create -f acm-config/dotnet-chart/deploy-local-cluster.yaml 
```

**Create dotnet-chart-dev project and deploy ssh key**
```
oc new-project  dotnet-chart-dev
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n  dotnet-chart-dev
```

**Deploy to dev cluster**
```
oc create -f acm-config/dotnet-chart/deploy-dev-cluster.yaml
```

### Steps to deploy nodejs-chart
**Create nodejs-chart project and deploy ssh key**
```
oc new-project nodejs-chart
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n nodejs-chart
```

**Deploy to local cluster**
```
oc create -f acm-config/nodejs-chart/deploy-local-cluster.yaml 
```

**Create nodejs-chart-dev project and deploy ssh key**
```
oc new-project  nodejs-chart-dev
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n  nodejs-chart-dev
```

**Deploy to dev cluster**
```
oc create -f acm-config/nodejs-chart/deploy-dev-cluster.yaml
```
### Steps to deploy nodejs-ex-k
**Create nodejs-ex-k project and deploy ssh key**
```
oc new-project nodejs-ex-k
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n nodejs-ex-k
```

**Deploy to local cluster**
```
oc create -f acm-config/nodejs-ex-k/deploy-local-cluster.yaml 
```

**Create nodejs-ex-k-dev project and deploy ssh key**
```
oc new-project  nodejs-ex-k-dev
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n  nodejs-ex-k-dev
```

**Deploy to dev cluster**
```
oc create -f acm-config/nodejs-ex-k/deploy-dev-cluster.yaml
```

### Steps to deploy quarkus-chart
**Create quarkus-chart project and deploy ssh key**
```
oc new-project quarkus-chart
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n quarkus-chart
```

**Deploy to local cluster**
```
oc create -f acm-config/quarkus-chart/deploy-local-cluster.yaml 
```

**Create quarkus-chart-dev project and deploy ssh key**
```
oc new-project  quarkus-chart-dev
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n  quarkus-chart-dev
```

**Deploy to dev cluster**
```
oc create -f acm-config/quarkus-chart/deploy-dev-cluster.yaml
```

### Steps to deploy wildfly-common
**Create wildfly-common project and deploy ssh key**
```
oc new-project wildfly-common
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n wildfly-common
```

**Deploy to local cluster**
```
oc create -f acm-config/wildfly-common/deploy-local-cluster.yaml 
```

**Create wildfly-common-dev project and deploy ssh key**
```
oc new-project  wildfly-common-dev
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n  wildfly-common-dev
```

**Deploy to dev cluster**
```
oc create -f acm-config/wildfly-common/deploy-dev-cluster.yaml
```

### Steps to deploy wildfly
**Create wildfly project and deploy ssh key**
```
oc new-project wildfly
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n wildfly
```

**Deploy to local cluster**
```
oc create -f acm-config/wildfly/deploy-local-cluster.yaml 
```

**Create wildfly-dev project and deploy ssh key**
```
oc new-project  wildfly-dev
oc create secret generic git-ssh-auth --from-file=sshKey=id_rsa -n  wildfly-dev
```

**Deploy to dev cluster**
```
oc create -f acm-config/wildfly/deploy-dev-cluster.yaml
```

## Links:  
* https://github.com/redhat-cop/helm-charts